import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import i18n from './i18n';

const locales = {
    en: require('vuetify/es5/locale/en'),
    fr: require('vuetify/es5/locale/fr'),
};

const current = navigator.language.split('-')[0];

Vue.use(Vuetify);

export default new Vuetify({
    lang: {
      t: (key, ...params) => i18n.t(key, params),
    },
    locales,
    current,
    icons: {
        iconfont: 'mdiSvg',
    },
    theme: {
        dark: true,
        themes: {
            dark: {
                primary: '#1689E7',
                accent: '#4CBB99',
                secondary: '#7BC6FF',
                success: '#4CAF50',
                info: '#2196F3',
                warning: '#FB8C00',
                error: '#FF5252',
            },
            light: {
                primary: '#1689E7',
                accent: '#4CBB99',
                secondary: '#7BC6FF',
                success: '#4CAF50',
                info: '#2196F3',
                warning: '#FB8C00',
                error: '#FF5252',
            },
        },
    },
});
