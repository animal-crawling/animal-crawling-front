import fr from 'vuetify/src/locale/fr.ts';

export default {
    welcome: 'Bienvenue !',
    darkMode: 'Thème sombre',
    lightMode: 'Thème lumineux',
    isRequired: 'est requis',
    animalSearch: 'Rechercher un animal',
    title: 'Titre',
    location: 'Localisation',
    price: 'Prix',
    minPrice: 'Prix minimum',
    maxPrice: 'Prix maximum',
    show: 'Voir plus',
    $vuetify: fr
};
