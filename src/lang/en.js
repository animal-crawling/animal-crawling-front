import en from 'vuetify/src/locale/en.ts';

export default {
    welcome: 'Welcome !',
    darkMode: 'Dark Mode',
    lightMode: 'Light mode',
    isRequired: 'is required',
    animalSearch: 'Search for an animal',
    title: 'Title',
    location: 'Location',
    price: 'Price',
    minPrice: 'Minimum price',
    maxPrice: 'Maximum price',
    show: 'Show more',
    $vuetify: en
};
