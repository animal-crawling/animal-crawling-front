# Animal Crawling - Front
![alt text](./src/assets/logo-small-w-padding.png "Animal Crawling Logo")

### Features : 
* Dashboard avec les annonces d'animaux enregistrées en base de données, triées par prix décroissant.
* Pagination, limitation d'éléments, recherche de titre, filtre prix min-max lié au back-end.
* Mode sombre / lumineux (non persistant)
* Traduction i18n intégrée, langue du navigateur par défaut et fallback sur l'anglais.
 
### Tech. featured

* [x] **[Vue](https://github.com/vuejs/vue)**
* [x] **[Vuetify](https://github.com/vuetifyjs/vuetify)**
* [x] **[Vuex](https://github.com/vuejs/vuex)**
* [x] **[Vue-router](https://github.com/vuejs/vue-router)**
* [x] **[Vue-i18n](https://github.com/kazupon/vue-i18n)**
* [x] **[Axios](https://github.com/axios/axios)** 

### Getting started
```
> npm i
> npm serve
```
